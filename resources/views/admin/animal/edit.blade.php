<h1>Edit {{ $animal->name }}</h1>
{!! Form::open(['url' => route('animals.update', ['id' => $animal->id]), 'method' => 'put']) !!}
	@include('admin.animal.form', ['animal' => $animal])
{!! Form::close() !!}
<a href="{{ route('animals.index') }}">List</a>
<a href="{{ route('animals.show', ['id' => $animal->id]) }}">Show</a>
