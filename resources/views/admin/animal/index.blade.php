<h1>Animal list</h1>
<ul>
	@foreach($animals as $animal)
		<li>
			<a href="{{ route('animals.show', ['id' => $animal->id]) }}">{{ $animal->name }}</a>
		</li>
	@endforeach
</ul>
<a href="{{ route('animals.create') }}">Create</a>
