<h1>{{ $animal->name }}</h1>
<table>
	<tr>
		<th>Name</th><td>{{ $animal->name }}</td>
	</tr>
	<tr>
		<th>Title</th><td>{{ $animal->title }}</td>
	</tr>
	<tr>
		<th>Description</th><td>{{ $animal->description }}</td>
	</tr>
	<tr>
		<th>Keywords</th><td>{{ $animal->keywords }}</td>
	</tr>
	<tr>
		<th>Content</th><td>{{ $animal->content }}</td>
	</tr>
</table>
<ul>
	@foreach($animal->images as $image)
		<li>{{ $image->name }}</li>
	@endforeach
</ul>
<a href="{{ route('animals.index') }}">List</a>
<a href="{{ route('animals.edit', ['id' => $animal->id]) }}">Edit</a>
{!! Form::open(['url' => route('animals.destroy', ['id' => $animal->id]), 'method' => 'delete']) !!}
	{{ Form::button('<i class="fas fa-times"></i>Delete', ['type' => 'submit']) }}
{!! Form::close() !!}
