<h1>Create animal</h1>
{!! Form::open(['url' => route('animals.store'), 'method' => 'post']) !!}
	@include('admin.animal.form')
{!! Form::close() !!}
<a href="{{ route('animals.index') }}">List</a>
