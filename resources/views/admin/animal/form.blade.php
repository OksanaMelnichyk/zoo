<div>
	{{ Form::label('image', 'Image') }}
	{{ Form::text('image', isset($animal) ? $animal->image : '') }}
</div> 

<div>
	{{ Form::label('name', 'Name') }}
	{{ Form::text('name', isset($animal) ? $animal->name : '') }}
</div>

<div>
	{{ Form::label('content', 'Content') }}
	{!! Form::textarea('content', isset($animal) ? $animal->content : '') !!}
</div>

{{ Form::button('<i class="fas fa-home"></i>Send', ['type' => 'submit']) }}
