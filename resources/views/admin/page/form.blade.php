<div> {{ Form::label('slug', 'Slug') }}
{{ Form::text('slug', isset($page) ? $page->slug : '') }} </div>

<div> {{ Form::label('name', 'Name') }}
{{ Form::text('name', isset($page) ? $page->name : '') }} </div>

<div>{{ Form::label('content', 'Content') }}
{!! Form::textarea('content', isset($page) ? $page->content : '') !!} </div>

{{ Form::button('<i class="fas fa-home"></i>Send', ['type' => 'submit']) }}
