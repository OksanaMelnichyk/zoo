{{ $page->name }}
{!! $page->content !!}
{!! Form::open(['url' => '/admin/pages/' . $page->id, 'method' => 'delete']) !!}
	{{ Form::button('<i class="fas fa-home"></i>Delete', ['type' => 'submit']) }}
{!! Form::close() !!}