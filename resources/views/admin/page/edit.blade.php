
<link rel="stylesheet" type="text/css" href="/plugins/fontawesome-free/css/fontawesome.min.css">
edit
{!! Form::open(['url' => '/admin/pages/' . $page->id, 'method' => 'put']) !!}
	@include('admin.page.form', ['page' => $page])
	
{!! Form::close() !!}