<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
    protected $fillable = ['image', 'name', 'title', 'description', 'keywords', 'content'];

    public function images()
    {
        return $this->hasMany('App\AnimalImage');
    }
}
