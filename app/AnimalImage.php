<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnimalImage extends Model
{
    protected $fillable = ['name', 'image', 'alt', 'description'];

    public function animal()
    {
        return $this->belongsTo('App\Animal');
    }
}
