<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            [
                'slug' => 'home',
                'name' => 'Главная',
                'content' => 'Добро пожаловать в наш зоопарк!',
            ],
            [
                'slug' => 'about',
                'name' => 'О нас ',
                'content' => 'Мы самые лучшие!',
            ],
            [
                'slug' => 'animal',
                'name' => 'Наши питомцы',
                'content' => ' ',
            ],
            [
                'slug' => 'price',
                'name' => 'Наши цены',
                'content' => ' ',
            ],
            [
                'slug' => 'contact',
                'name' => 'Контакты',
                'content' => 'Мы в ваших сердцах!',
            ],
            
        ]);
    }
}

