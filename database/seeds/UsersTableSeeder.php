<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'admin',
                'email' => 'admin1@gmail.com',
                'password' => '$2y$10$ptXVSdKObpUfM9.bBNIE4uB4OSiD4PZ7l8hoxGvMe5qUdnOlwLJe.'
            ],
            [
                'name' => 'user1',
                'email' => 'user1@gmail.com',
                'password' => '$2y$10$ptXVSdKObpUfM9.bBNIE4uB4OSiD4PZ7l8hoxGvMe5qUdnOlwLJe.'
            ],   
        ]);
    }
}

