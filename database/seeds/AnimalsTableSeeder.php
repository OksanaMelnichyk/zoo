<?php

use Illuminate\Database\Seeder;

class AnimalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('animals')->insert([
            [
                'image' => 'rabbit.png',
                'name' => 'Кролики',
                'content' => 'Кролики',
            ],
            [
                'image' => '',
                'name' => 'Носухи',
                'content' => 'Носухи',
            ],
            [
                'image' => '',
                'name' => 'Саймири',
                'content' => 'Саймири',
            ],
            [
                'image' => '',
                'name' => 'Птицы',
                'content' => 'Птицы',
            ],
            [
                'image' => '',
                'name' => 'Сурикаты',
                'content' => 'Сурикаты',
            ],
            [
                'image' => '',
                'name' => 'Улитки',
                'content' => 'Улитки',
            ],
  
        ]);
    }
}
